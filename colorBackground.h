#ifndef OPENGLWINDOW_COLORBACKGROUND_H
#define OPENGLWINDOW_COLORBACKGROUND_H

#include "guiBackground.h"

#include "colorBackgroundStyle.h"

#include <glm/vec4.hpp>

namespace DGUI
{
	class ColorBackground
			: public GUIBackground
	{
	public:
		ColorBackground();
		~ColorBackground();

		void Init(GUIStyle* style, const DLib::Rect& area) override;
		void Draw(DLib::SpriteRenderer* spriteRenderer) override;

		virtual GUIBackground* Clone() override;

	private:
		ColorBackgroundStyle* style;
	};
}

#endif //OPENGLWINDOW_COLORBACKGROUND_H
