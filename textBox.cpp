#include "textBox.h"

#include <dlib/logger.h>
#include <dlib/input.h>

#include <utf8-cpp/utf8.h>

DGUI::TextBox::TextBox()
	: GUIContainer()
	, drawCursor(false)
    , cursorIndex(0)
	, allowEdit(true)
    , mouseDown(false)
	, selectionIndex(-1)
	, selectionStartIndex(-1)
	, selectionEndIndex(-1)
	, xOffset(0.0f)
{
	active = false;
}

DGUI::TextBox::~TextBox()
{
}

void DGUI::TextBox::Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle)
{
	this->style = CastStyleTo<TextBoxStyle>(style, DLib::LOG_TYPE_ERROR);
	if(this->style == nullptr)
		return;
	else if(this->style->characterSet == nullptr)
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_ERROR, "TextBoxStyle->characterSet is nullptr!");
		return;
	}

	GUIContainer::Init(area
					   , style
					   , background
					   , backgroundStyle);

	cursorColor = this->style->cursorColorNormal;

	SetText("");
}

void DGUI::TextBox::Update(std::chrono::nanoseconds delta)
{
	if(mouseDown)
	{
		if(DLib::Input::MouseMoved())
		{
			glm::vec2 mousePos = DLib::Input::GetMousePosition();

			if(SelectionMade())
			{
				unsigned int newSelectionIndex = style->characterSet->GetIndexAtWidth(constructedString, static_cast<int>(mousePos.x - area.GetMinPosition().x -xOffset));

				SetCursorIndex(newSelectionIndex);
				ExtendSelectionToCursor();
				SetXOffset();
			}
			else
			{
				unsigned int index = style->characterSet->GetIndexAtWidth(constructedString, static_cast<int>(mousePos.x - area.GetMinPosition().x + -xOffset));

				if(index != cursorIndex)
				{
					BeginSelection();
					SetCursorIndex(index);
					ExtendSelectionToCursor();
				}
			}
		}
	}
}

/*void DGUI::TextBox::Draw(DLib::SpriteRenderer* spriteRenderer)
{
	if(cursorBlinkTimer.GetTime() >= cursorBlinkTime)
	{
		cursorBlinkTimer.Reset();
		cursorColor = (cursorColor == style->cursorColorNormal ? style->cursorColorBlink : style->cursorColorNormal);
	}

	spriteRenderer->EnableScissorTest(area);
	spriteRenderer->Draw(area, style->backgroundColorNormal);

	if(constructedString != "")
	{
		//Draw text
		if(SelectionMade())
		{
			unsigned int selectionMinX = characterSet->GetWidthAtIndex(constructedString, selectionStartIndex);
			unsigned int selectionMaxX = characterSet->GetWidthAtIndex(constructedString, selectionEndIndex);

			//Selection highlight
			spriteRenderer->Draw(DLib::Rect(area.GetMinPosition() + glm::vec2(xOffset + selectionMinX, 0.0f), static_cast<float>(selectionMaxX - selectionMinX), area.GetHeight()), style->backgroundColorSelected);

			spriteRenderer->DrawString(characterSet, constructedString, area.GetMinPosition() + glm::vec2(xOffset, 0.0f), 0, selectionStartIndex, style->textColorNormal); //Text before selection
			spriteRenderer->DrawString(characterSet, constructedString, area.GetMinPosition() + glm::vec2(xOffset + selectionMinX, 0.0f), selectionStartIndex, selectionEndIndex - selectionStartIndex, style->textColorSelected); //Selected text
			spriteRenderer->DrawString(characterSet, constructedString, area.GetMinPosition() + glm::vec2(xOffset + selectionMaxX, 0.0f), selectionEndIndex, static_cast<unsigned int>(constructedString.text.size()), style->textColorNormal); //Text after selection
		}
		else
			spriteRenderer->DrawString(characterSet, constructedString, area.GetMinPosition() + glm::vec2(xOffset, 0.0f), style->textColorNormal);

		//Draw cursor
		if(drawCursor)
		{
			int width = characterSet->GetWidthAtIndex(constructedString, cursorIndex);
			spriteRenderer->Draw(DLib::Rect(area.GetMinPosition() + style->cursorOffset + glm::vec2(width + xOffset, 0.0f), style->cursorSize), cursorColor);
		}
	}
	else if(drawCursor)
	{
		spriteRenderer->Draw(DLib::Rect(area.GetMinPosition() + style->cursorOffset, style->cursorSize), cursorColor);
	}

	spriteRenderer->DisableScissorTest();
}*/

void DGUI::TextBox::DrawBackground(DLib::SpriteRenderer* spriteRenderer)
{
	background->Draw(spriteRenderer);
	//spriteRenderer->Draw(area, style->backgroundColorNormal);
}

void DGUI::TextBox::DrawMiddle(DLib::SpriteRenderer* spriteRenderer)
{
	spriteRenderer->EnableScissorTest(area);
	//TODO: Move selection highlight to DrawBackground?
	if(constructedString != "")
	{
		//Draw text
		if(SelectionMade())
		{
			unsigned int selectionMinX = style->characterSet->GetWidthAtIndex(constructedString, selectionStartIndex);
			unsigned int selectionMaxX = style->characterSet->GetWidthAtIndex(constructedString, selectionEndIndex);

			//Selection highlight
			spriteRenderer->Draw(DLib::Rect(area.GetMinPosition() + glm::vec2(xOffset + selectionMinX, 0.0f), static_cast<float>(selectionMaxX - selectionMinX), area.GetHeight()), style->textHightlightCOlor);

			spriteRenderer->DrawString(style->characterSet, constructedString, area.GetMinPosition() + glm::vec2(xOffset, 0.0f), 0, selectionStartIndex, style->textColorNormal); //Text before selection
			spriteRenderer->DrawString(style->characterSet, constructedString, area.GetMinPosition() + glm::vec2(xOffset + selectionMinX, 0.0f), selectionStartIndex, selectionEndIndex - selectionStartIndex, style->textColorSelected); //Selected text
			spriteRenderer->DrawString(style->characterSet, constructedString, area.GetMinPosition() + glm::vec2(xOffset + selectionMaxX, 0.0f), selectionEndIndex, static_cast<unsigned int>(constructedString.text.size()), style->textColorNormal); //Text after selection
		}
		else
			spriteRenderer->DrawString(style->characterSet, constructedString, area.GetMinPosition() + glm::vec2(xOffset, 0.0f), style->textColorNormal); //TODO: Improve this, use ranged drawing instead
	}
	spriteRenderer->DisableScissorTest();
}

void DGUI::TextBox::DrawForeground(DLib::SpriteRenderer* spriteRenderer)
{
	if(drawCursor)
	{
		if(cursorBlinkTimer.GetTime() >= cursorBlinkTime)
		{
			cursorBlinkTimer.Reset();
			cursorColor = (cursorColor == style->cursorColorNormal ? style->cursorColorBlink : style->cursorColorNormal);
		}

		if(constructedString != "")
		{
			int width = style->characterSet->GetWidthAtIndex(constructedString, cursorIndex);
			spriteRenderer->Draw(DLib::Rect(area.GetMinPosition() + style->cursorOffset + glm::vec2(width + xOffset, 0.0f), style->cursorSize), cursorColor);
		}
		else
			spriteRenderer->Draw(DLib::Rect(area.GetMinPosition() + style->cursorOffset, style->cursorSize), cursorColor);
	}
}

void DGUI::TextBox::Insert(int index, unsigned int character)
{
	if(SelectionMade())
		EraseSelection();
	
	style->characterSet->Insert(constructedString, index, character);

	cursorIndex++;
	SetXOffset();
}

void DGUI::TextBox::Insert(unsigned int index, const std::string& text)
{
	if(SelectionMade())
		EraseSelection();

	style->characterSet->Insert(constructedString, index, text);

	cursorIndex += static_cast<int>(utf8::unchecked::distance(text.begin(), text.end()));
	SetXOffset();
}

void DGUI::TextBox::Insert(unsigned int index, const char* text)
{
	std::string textString(text);
	Insert(index, textString);
}

void DGUI::TextBox::Erase(unsigned int startIndex, unsigned int count)
{
	style->characterSet->Erase(constructedString, startIndex, count);

	if(count > 0)
	{
		cursorIndex -= count;

		if(cursorIndex < 0)
			SetCursorIndex(0);
	}

	SetXOffset();
}

void DGUI::TextBox::SetText(const std::string& text)
{
	if(!style->characterSet->IsLoaded())
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_ERROR, "Trying to set text to \"" + text + "\" before setting a font!");

		return;
	}

	constructedString = style->characterSet->ConstructString(text);
	SetCursorIndex(static_cast<unsigned int>(utf8::unchecked::distance(text.begin(), text.end())));

	SetXOffset();
}

std::string DGUI::TextBox::GetText()
{
	return constructedString.text;
}

std::string DGUI::TextBox::GetSelectedText()
{
	if(!SelectionMade())
		return "";

	auto beginIter = constructedString.text.begin();
	for(int i = 0; i < selectionStartIndex; i++)
		utf8::unchecked::next(beginIter);

	auto endIter = beginIter;
	for(int i = selectionStartIndex; i < selectionEndIndex; i++)
		utf8::unchecked::next(endIter);

	std::string returnString = constructedString.text.substr(static_cast<unsigned int>(std::distance(constructedString.text.begin(), beginIter)), static_cast<unsigned int>(std::distance(beginIter, endIter)));

	return returnString;
}

void DGUI::TextBox::SetCharacterSet(const std::string& fontPath, DLib::ContentManager* contentManager)
{
	SetCharacterSet(contentManager->Load<DLib::CharacterSet>(fontPath));
}

void DGUI::TextBox::SetCharacterSet(const DLib::CharacterSet* characterSet)
{
	style->characterSet = characterSet;

	constructedString = style->characterSet->ConstructString(constructedString.text);
}

void DGUI::TextBox::SetCutoffString(const std::string& string)
{
	constructedCutoffString = style->characterSet->ConstructString(string);
}
void DGUI::TextBox::OnMouseEnter()
{
	active = true;
}

void DGUI::TextBox::OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(keyState.key == GLFW_MOUSE_BUTTON_LEFT)
	{
		if(keyState.action == GLFW_PRESS)
		{
			if(!area.Contains(mousePosition))
			{
				active = false;

				drawCursor = false;
			}
			else
			{
				cursorBlinkTimer.Reset();
				cursorBlinkTimer.Start();

				cursorColor = style->cursorColorNormal;

				drawCursor = true;

				unsigned int newCursorIndex = style->characterSet->GetIndexAtWidth(constructedString, static_cast<unsigned int>(mousePosition.x - area.GetMinPosition().x + -(xOffset)));

				if(keyState.mods == GLFW_MOD_SHIFT)
				{
					if(!SelectionMade())
					{
						BeginSelection();
						SetCursorIndex(newCursorIndex);
						ExtendSelectionToCursor();
					}
					else
					{
						SetCursorIndex(newCursorIndex);
						ExtendSelectionToCursor();
					}
				}
				else
				{
					Deselect();
					SetCursorIndex(newCursorIndex);
				}

				mouseDown = true;
			}
		}
		else if(keyState.action == GLFW_DOUBLE_CLICK)
		{
			if(!SelectionMade())
			{
				JumpCursorLeft();
				BeginSelection();
				JumpCursorRight();
				ExtendSelectionToCursor();
			}
		}
	}
}

void DGUI::TextBox::OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(keyState.key == GLFW_MOUSE_BUTTON_LEFT
		&& keyState.action == GLFW_RELEASE)
		mouseDown = false;
}

void DGUI::TextBox::OnKeyDown(const DLib::KeyState& keyState)
{
	if(drawCursor)
	{
		cursorBlinkTimer.Reset();
		cursorColor = style->cursorColorNormal;

		switch(keyState.key)
		{
			case GLFW_KEY_BACKSPACE:
				if(allowEdit)
					BackspacePressed(keyState);
				break;
			case GLFW_KEY_LEFT:
				LeftPressed(keyState);
				break;
			case GLFW_KEY_RIGHT:
				RightPressed(keyState);
				break;
			case GLFW_KEY_DELETE:
				if(allowEdit)
					DeletePressed(keyState);
				break;
			case GLFW_KEY_X:
				if(keyState.mods == GLFW_MOD_CONTROL)
				{
					glfwSetClipboardString(DLib::Input::GetListenWindow(), GetSelectedText().c_str());

					if(allowEdit)
					{
						EraseSelection();
						SetXOffset();
					}
				}
				break;
			case GLFW_KEY_C:
				if(keyState.mods == GLFW_MOD_CONTROL)
				{
					glfwSetClipboardString(DLib::Input::GetListenWindow(), GetSelectedText().c_str());
				}
				break;
			case GLFW_KEY_V:
				if(keyState.mods == GLFW_MOD_CONTROL)
				{
					const char* text = glfwGetClipboardString(DLib::Input::GetListenWindow());

					if(text != NULL
						&& allowEdit)
					{
						Insert(cursorIndex, text);
						SetXOffset();
					}
				}
				break;
			case GLFW_KEY_A:
				if(keyState.mods == GLFW_MOD_CONTROL)
				{
					SetCursorIndex(0);
					BeginSelection();
					SetCursorIndex(constructedString.utf8Length);
					ExtendSelectionToCursor();
					SetXOffset();
				}
				break;
			case GLFW_KEY_HOME:
				if(keyState.mods == GLFW_MOD_SHIFT)
				{
					if(!SelectionMade())
						BeginSelection();

					SetCursorIndex(0);

					ExtendSelectionToCursor();
				}
				else
					SetCursorIndex(0);

				SetXOffset();
				break;
			case GLFW_KEY_END:

				if(keyState.mods == GLFW_MOD_SHIFT)
				{
					if(!SelectionMade())
						BeginSelection();

					SetCursorIndex(constructedString.utf8Length);

					ExtendSelectionToCursor();
				}
				else
					SetCursorIndex(constructedString.utf8Length);

				SetXOffset();
				break;
			default:
				break;
		}
	}
}

void DGUI::TextBox::OnKeyUp(const DLib::KeyState& keyState)
{

}

void DGUI::TextBox::OnChar(unsigned int keyCode)
{
	if(drawCursor && allowEdit)
	{
		if(!SelectionMade())
			Insert(cursorIndex, keyCode);
		else
			Insert(selectionStartIndex, keyCode);

		cursorBlinkTimer.Reset();
		cursorColor = style->cursorColorNormal;
	}
}

void DGUI::TextBox::LeftPressed(const DLib::KeyState& keyState)
{
	switch(keyState.mods)
	{
		case 0:
			if(!SelectionMade())
			{
				MoveCursorLeft();
				SetXOffset();
			}
			else
			{
				int newCursorIndex = selectionStartIndex;
				Deselect();
				SetCursorIndex(newCursorIndex);
			}
			break;
		case GLFW_MOD_SHIFT:
			if(!SelectionMade())
				BeginSelection();

			MoveCursorLeft();
			ExtendSelectionToCursor();
			SetXOffset();
			break;
		case GLFW_MOD_CONTROL:
			Deselect();
			JumpCursorLeft();
			SetXOffset();
			break;
		case GLFW_MOD_ALT:
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_CONTROL:
			if(!SelectionMade())
				BeginSelection();

			JumpCursorLeft();
			ExtendSelectionToCursor();
			SetXOffset();
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_ALT:
			break;
		case GLFW_MOD_CONTROL | GLFW_MOD_ALT:
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_CONTROL | GLFW_MOD_ALT:
			break;
		default:
			break;
	}
}

void DGUI::TextBox::RightPressed(const DLib::KeyState& keyState)
{
	switch(keyState.mods)
	{
		case 0:
			if(!SelectionMade())
			{
				MoveCursorRight();
				SetXOffset();
			}
			else
			{
				int newCursorIndex = selectionEndIndex;
				Deselect();
				SetCursorIndex(newCursorIndex);
			}
			break;
		case GLFW_MOD_SHIFT:
			if(!SelectionMade())
				BeginSelection();

			MoveCursorRight();
			ExtendSelectionToCursor();
			SetXOffset();
			break;
		case GLFW_MOD_CONTROL:
			Deselect();
			JumpCursorRight();
			SetXOffset();
			break;
		case GLFW_MOD_ALT:
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_CONTROL:
			if(!SelectionMade())
				BeginSelection();

			JumpCursorRight();
			ExtendSelectionToCursor();
			SetXOffset();
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_ALT:
			break;
		case GLFW_MOD_CONTROL | GLFW_MOD_ALT:
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_CONTROL | GLFW_MOD_ALT:
			break;
		default:
			break;
	}
}

void DGUI::TextBox::BackspacePressed(const DLib::KeyState& keyState)
{
	if(keyState.mods == GLFW_MOD_CONTROL)
	{
		BeginSelection();
		JumpCursorLeft();
		ExtendSelectionToCursor();
		EraseSelection();
	}
	else
	{
		if(SelectionMade())
			EraseSelection();
		else
		{
			auto iter = constructedString.text.begin();
			utf8::unchecked::advance(iter, cursorIndex - 1); //I just discovered advance(). Horray for me...

			style->characterSet->Erase(constructedString, cursorIndex - 1, 1);

			MoveCursorLeft();
			unsigned int index = cursorIndex;

			JumpCursorLeft();
			SetXOffset();
			SetCursorIndex(index);
		}
	}

	SetXOffset();
}

void DGUI::TextBox::DeletePressed(const DLib::KeyState& keyState)
{
	if(keyState.mods == GLFW_MOD_CONTROL)
	{
		BeginSelection();
		JumpCursorRight();
		ExtendSelectionToCursor();
		EraseSelection();
	}
	else
	{
		if(SelectionMade())
			EraseSelection();
		else
			style->characterSet->Erase(constructedString, cursorIndex, 1);
	}

	SetXOffset();
}

void DGUI::TextBox::SetCursorIndex(unsigned int newIndex)
{
	//Unsigned int means there's no need to check values below 0

	if(newIndex > constructedString.utf8Length)
		newIndex = constructedString.utf8Length;

	cursorIndex = newIndex;
}

void DGUI::TextBox::MoveCursorLeft()
{
	if(cursorIndex > 0)
		cursorIndex--;
}

void DGUI::TextBox::MoveCursorRight()
{
	if(cursorIndex < static_cast<int>(constructedString.utf8Length))
		cursorIndex++;
}

void DGUI::TextBox::JumpCursorLeft()
{
	auto iter = constructedString.text.begin();
	utf8::unchecked::advance(iter, cursorIndex - 2);

	bool nonSpace = utf8::unchecked::next(iter) != DLib::CharacterSet::SPACE_CHARACTER;

	for(MoveCursorLeft(); cursorIndex > 0; cursorIndex--)
	{
		if(utf8::unchecked::prior(iter) == DLib::CharacterSet::SPACE_CHARACTER)
		{
			if(nonSpace)
				break;
		}
		else
			nonSpace = true;
	}
}

void DGUI::TextBox::JumpCursorRight()
{
	auto iter = constructedString.text.begin();
	utf8::unchecked::advance(iter, cursorIndex);

	if(iter == constructedString.text.end())
		return;

	bool nonSpace = utf8::unchecked::next(iter) != DLib::CharacterSet::SPACE_CHARACTER;

	for(MoveCursorRight(); cursorIndex < constructedString.utf8Length; cursorIndex++)
	{
		if(utf8::unchecked::next(iter) == DLib::CharacterSet::SPACE_CHARACTER)
		{
			if(nonSpace)
				break;
		}
		else
			nonSpace = true;
	}
}

void DGUI::TextBox::ExtendSelectionToCursor()
{
	if(cursorIndex > selectionIndex)
	{
		selectionStartIndex = selectionIndex;
		selectionEndIndex = cursorIndex;
	}
	else
	{
		selectionStartIndex = cursorIndex;
		selectionEndIndex = selectionIndex;
	}
}

void DGUI::TextBox::BeginSelection()
{
	selectionIndex = cursorIndex;
	selectionStartIndex = selectionIndex;
}

void DGUI::TextBox::Deselect()
{
	selectionEndIndex = -1;
	selectionStartIndex = -1;
}

void DGUI::TextBox::EraseSelection()
{
	if(SelectionMade())
	{
		style->characterSet->Erase(constructedString, selectionStartIndex, selectionEndIndex - selectionStartIndex);
		SetCursorIndex(selectionStartIndex);
		Deselect();
	}
}

bool DGUI::TextBox::SelectionMade()
{
	return selectionStartIndex != -1 && selectionEndIndex != -1;
}

void DGUI::TextBox::SetXOffset()
{
	int widthAtCursor = style->characterSet->GetWidthAtIndex(constructedString, cursorIndex);

	if(widthAtCursor > area.GetWidth() + (-xOffset))
		xOffset = -(widthAtCursor - area.GetWidth());
	else if(widthAtCursor < -xOffset)
		xOffset = static_cast<float>(-widthAtCursor);
}

bool DGUI::TextBox::IsEmpty()
{
	return constructedString.text.empty();
}
