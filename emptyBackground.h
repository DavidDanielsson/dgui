#ifndef OPENGLWINDOW_EMPTYBACKGROUND_H
#define OPENGLWINDOW_EMPTYBACKGROUND_H

#include "guiBackground.h"

namespace DGUI
{
	class EmptyBackground
			: public GUIBackground
	{
	public:
		EmptyBackground();
		~EmptyBackground();

		void Init(GUIStyle* style, const DLib::Rect& area) override;
		void Draw(DLib::SpriteRenderer* spriteRenderer) override;
	};
}

#endif //OPENGLWINDOW_EMPTYBACKGROUND_H
