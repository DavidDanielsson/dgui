#ifndef Scrollbar_h__
#define Scrollbar_h__

#include "guiContainer.h"

#include "scrollbarStyle.h"

namespace DGUI
{
	class Scrollbar :
		public GUIContainer
	{
	public:
		Scrollbar();
		~Scrollbar();

		virtual void Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle) override;
		virtual void Update(std::chrono::nanoseconds delta) override;
		virtual void DrawBackground(DLib::SpriteRenderer* spriteRenderer);
		virtual void DrawMiddle(DLib::SpriteRenderer* spriteRenderer);

		void Scroll(int index);
		void ScrollTo(int index);

		//void SetStyle(ScrollbarStyle style);
		void SetVisibleItems(int visibleItems);
		void SetMaxItems(int maxItems);

		int GetVisibleItems() const;
		int GetMaxItems() const;

		int GetMinIndex() const;
		int GetMaxIndex() const;

		virtual void SetPosition(const glm::vec2& newPosition) override;
		virtual void SetPosition(float x, float y) override;
		virtual void SetSize(const glm::vec2& newSize) override;
		virtual void SetSize(float x, float y) override;
		virtual void SetArea(const DLib::Rect& newArea) override;

		virtual void OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnScroll(double x, double y) override;

		bool Contains(const glm::vec2& position) const;
	protected:
		bool mouseDown;
		bool lockedToBottom;

		int beginIndex;
		int endIndex;

		int visibleItems;
		int maxItems;

		//////////////////////////////////////////////////////////////////////////
		//THUMB
		//////////////////////////////////////////////////////////////////////////
		DLib::Rect thumb;

		int grabY;

		ScrollbarStyle* style;

		//************************************
		// Method:      UpdateThumbSize
		// FullName:    DGUI::Scrollbar::UpdateThumbSize
		// Access:      protected 
		// Returns:     void
		// Qualifier:  
		// Description: Updates the thumb size based on visible items and max items
		//************************************
		void UpdateThumbSize();
		//************************************
		// Method:      UpdateThumbIndex
		// FullName:    DGUI::Scrollbar::UpdateThumbIndex
		// Access:      protected 
		// Returns:     void
		// Qualifier:  
		// Description: Updates the thumb index based on its position
		//************************************
		void UpdateThumbIndex();
		//************************************
		// Method:      UpdateThumbPosition
		// FullName:    DGUI::Scrollbar::UpdateThumbPosition
		// Access:      protected 
		// Returns:     void
		// Qualifier:  
		// Description: Updates the thumb position based on its index
		//************************************
		void UpdateThumbPosition();

		void ClampThumbPosition();

		int GetEffectiveHeight();
		float GetThumbRelativeY();
	};
}

#endif // Scrollbar_h__
