#ifndef List_h__
#define List_h__

#include "guiContainer.h"
#include "guiManager.h"
#include "scrollbar.h"

#include "listStyle.h"

#include <vector>

namespace DGUI
{
	class List :
		public DGUI::GUIContainer
	{
	public:
		List();
		virtual ~List();

		//Tell David if you have a good reason to copy
		List& operator=(const List&) = delete;
		List(const List&) = delete;

		virtual void Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle) override;
		virtual void Update(std::chrono::nanoseconds delta) override;

		virtual void Draw(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void DrawBackground(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void DrawMiddle(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void DrawForeground(DLib::SpriteRenderer* spriteRenderer) override;

		virtual void SetPosition(const glm::vec2& newPosition) override;
		virtual void SetPosition(float x, float y) override;

		virtual void SetSize(const glm::vec2& newSize) override;
		virtual void SetSize(float x, float y) override;
		virtual void SetArea(const DLib::Rect& newArea) override;

		void AddElement(GUIContainer* element);
		void SetElements(const std::vector<GUIContainer*>& elements);
		void ClearElements();

		virtual void OnMouseEnter() override;
		virtual void OnMouseExit() override;
		virtual void OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnKeyDown(const DLib::KeyState& keyState) override;
		virtual void OnKeyUp(const DLib::KeyState& keyState) override;
		virtual void OnChar(unsigned int keyCode) override;
		virtual void OnScroll(double x, double y) override;
	private:
		std::vector<GUIContainer*> elements;

		GUIContainer* focusOn;

		ListStyle* style;

		GUIManager manager;

		Scrollbar scrollbar;
		int drawBegin;
		int drawEnd;

		int elementHeight;

		bool mouseDown;

		void UpdatePositions();
		void UpdateSizes();
	};
}

#endif // List_h__
