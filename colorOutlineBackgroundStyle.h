#ifndef OPENGLWINDOW_COLOROUTLINEBACKGROUNDSTYLE_H
#define OPENGLWINDOW_COLOROUTLINEBACKGROUNDSTYLE_H

#include "guiStyle.h"

#include "common.h"

#include <glm/vec4.hpp>

namespace DGUI
{
	struct ColorOutlineBackgroundStyle
			: public GUIStyle
	{
	public:
		ColorOutlineBackgroundStyle()
				: outlineThickness(1.0f)
				, excludeSides(0)
		{ };
		~ColorOutlineBackgroundStyle()
		{ };

		std::vector<glm::vec4> outlineColors;
		std::vector<glm::vec4> backgroundColors;

		//Has to be float because only vec4 + float is valid
		float outlineThickness;

		int excludeSides;
	};
}

#endif //OPENGLWINDOW_COLOROUTLINEBACKGROUNDSTYLE_H
