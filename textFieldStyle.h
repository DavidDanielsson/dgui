#ifndef TextFieldStyle_h__
#define TextFieldStyle_h__

#include "guiStyle.h"

#include "scrollbarStyle.h"

#include <dlib/rect.h>

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

namespace DGUI
{
	struct TextFieldStyle
		: public GUIStyle
	{
		TextFieldStyle()
			: textHighlightColor(173.0f / 255.0f, 214.0f / 255.0f, 1.0f, 1.0f)
			, textColorNormal(0.0f, 0.0f, 0.0f, 1.0f)
			, textColorSelected(0.0f, 0.0f, 0.0f, 1.0f)
			, cursorSize(0.0f, 0.0f)
			, cursorColorNormal(0.0f, 0.0f, 0.0f, 1.0f)
			, cursorColorBlink(0.0f, 0.0f, 0.0f, 0.0f)
			, cursorOffset(0.0f, 0.0f)
			, scrollBarPadding(5)
		{};
		virtual ~TextFieldStyle() {};

		glm::vec4 textHighlightColor;
		glm::vec4 textColorNormal;
		glm::vec4 textColorSelected;

		glm::vec2 cursorSize;
		glm::vec4 cursorColorNormal;
		glm::vec4 cursorColorBlink;
		glm::vec2 cursorOffset;

		ScrollbarStyle* scrollBarStyle;

		const DLib::CharacterSet* characterSet;

		//Padding between the text and the scrollbar
		int scrollBarPadding;

		GUIBackground* scrollbarBackground;
		GUIStyle* scrollbarBackgroundStyle;
	};
}

#endif // TextFieldStyle_h__
