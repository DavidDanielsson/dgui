#include "colorBackground.h"

DGUI::ColorBackground::ColorBackground()
{

}

DGUI::ColorBackground::~ColorBackground()
{

}

void DGUI::ColorBackground::Init(GUIStyle* style, const DLib::Rect& area)
{
	this->style = CastStyleTo<ColorBackgroundStyle>(style);
	this->area = area;
}


void DGUI::ColorBackground::Draw(DLib::SpriteRenderer* spriteRenderer)
{
	spriteRenderer->Draw(area, style->colors[preset]);
}

DGUI::GUIBackground* DGUI::ColorBackground::Clone()
{
	ColorBackground* returnBackground = new ColorBackground(*this);

	return returnBackground;
}
