#ifndef FormattedString_h__
#define FormattedString_h__

#include <dlib/constructedString.h>
#include <dlib/characterSet.h>

namespace DGUI
{
	class FormattedString 
		: public DLib::ConstructedString
	{
	public:
		FormattedString();
		virtual ~FormattedString();
	};
}

#endif // FormattedString_h__
